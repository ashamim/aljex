<?php
    // Author: Akhtar Shamim
    // Project: Aljex Http data post
    // Created on: 05/14/2014 11:00 am
    // Modified on: 09/01/2015 03:45 pm

    if (!empty($_POST))
    {
        // Connect to Oracle
        $conn = oci_connect('wexnet', 'wexnet1', '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=db3-vip)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=db4-vip)(PORT=1521))(LOAD_BALANCE=yes)(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=wwex)))');
        if (!$conn) {
            echo "Not Connected to oracle";
            $e = oci_error();
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

        $REF_Table = "";
        $REF_Table_ID = 0;
        $REF_Table_Status = "";
        // Make Data string |KEY:VALUE|
        $HTTP_DATA .= "|";
        foreach ($_POST as $key => $value){
            // if($key == "web_sync_table_name") { $REF_Table = $value; }
            // if($key == "id") { $REF_Table_ID = $value; }
            // if($key == "status") { $REF_Table_Status  = $value; }            
            $HTTP_DATA .= "$key:$value|";
        }

        //$file = 'aljex.txt';
		//$current = file_get_contents($file);
		//$current .= "$HTTP_DATA\n";
		//file_put_contents($file, $current);

        // Save Data in Oracle Table
        //$query = ("begin insert into wexnet.aljex_http_data (HTTP_DATA,RECEIVED_ON,PROCESSED,REF_TABLE, REF_TABLE_ID) values ('$HTTP_DATA',SYSTIMESTAMP,'N','$REF_Table','$REF_Table_ID'); end;");
		$query = ("Begin wexnet.PKG_ALJEX_HTTP_DATA.PR_RECEIVE_HTTP_DATA('$HTTP_DATA'); End;");
		//$query = ("begin insert into wexnet.aljex_http_data (HTTP_DATA,RECEIVED_ON,PROCESSED) values ('$HTTP_DATA',SYSTIMESTAMP,'N'); end;");
        $stid = oci_parse($conn, $query);
        try {
		    oci_execute($stid);
		    echo 'Data received, Thanks.';

            /*
            $Chn = 'test-ping';
            $Usr = 'lion';
            $Icn = 'lion_face';
            $Msg = $REF_Table . " " . $REF_Table_ID . " " . $REF_Table_Status;
            // Create a constant to store your Slack URL
            define(
                'SLACK_WEBHOOK',
                'https://hooks.slack.com/services/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'
            );
            // Make your message
            $message = array(
                'payload' => json_encode(
                    array(
                        'channel' => $Chn,
                        'username' => $Usr,
                        'icon_emoji' => ':' . $Icn . ':',
                        'text' => $Msg
                    )
                )
            );            
            // Use curl to send your message
            $c = curl_init(SLACK_WEBHOOK);
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($c, CURLOPT_POST, true);
            curl_setopt($c, CURLOPT_POSTFIELDS, $message);
            curl_exec($c);
            curl_close($c);
            */

		} catch(Exception $e) {
			//file_put_contents($file, $e['message']);
		    echo $e['message'];
		}
    }
    else // $_POST is empty.
    {
        echo "No Post Variables found, please check and resubmit form. Thanks.";
    }
?>
